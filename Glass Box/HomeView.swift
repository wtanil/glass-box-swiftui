//
//  HomeView.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 04/12/20.
//

import SwiftUI

struct HomeView: View {
    var body: some View {
        NavigationView {
            Text("Hello World")
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
