//
//  Glass_BoxApp.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 04/12/20.
//

import SwiftUI

@main
struct Glass_BoxApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
