//
//  BulkCreateView.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 04/12/20.
//

import SwiftUI

struct BulkCreateView: View {
    var body: some View {
        NavigationView {
            NavigationLink(
                destination: HomeView(),
                label: {
                    Text("Add aquarium")
                })
            
            NavigationLink(
                destination: HomeView(),
                label: {
                    Text("Add event")
                })
            
            NavigationLink(
                destination: HomeView(),
                label: {
                    Text("Add parameter")
                })
        }
    }
}

struct BulkCreateView_Previews: PreviewProvider {
    static var previews: some View {
        BulkCreateView()
    }
}
