//
//  TabTagType.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 04/12/20.
//

import Foundation

enum TabTagType {
    case home, create
    case text1, text2
}
